<?php

namespace SoapMedia\DeployBundle\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Process\Process;

class DeployCommand extends Command {
    private $env;

    private $helper;

    private $commands;

    public function __construct($name = null)
    {
        $this->helper = new QuestionHelper();

        $this->commands = array();

        parent::__construct($name);
    }


    protected function configure() {
        $this
            ->setName("leadzenV2:deploy")
            ->setDescription("Deploy the leadzenV2 app")
            ->addOption('env', null, InputOption::VALUE_OPTIONAL, 'What env are you in dev or prod. Defaults to test', 'test')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->env = $input->getOption("env");

        $output->write("You are about to deploy LeadZenV2 app to ");
        if ($this->env == "prod") {
            $output->writeln("<error>".$this->env."</error>");
        } else {
            $output->writeln("<info>".$this->env."</info>");
        }

        $question = new ConfirmationQuestion("Continue? <info>(y/n)</info> ");
        $continue = $this->helper->ask($input, $output, $question);

        $this->addCommand("cd C:\xampp\htdocs\leadzen-v2");

        $this->addCommand('git config --global url."https://".insteadOf git://');

        if ($this->env == "prod") {
            $this->addCommand("echo 'Pulling Master Branch'");
            $this->addCommand("git pull origin master");
        } else {
            $this->addCommand("echo 'Pulling Dev Branch'");
            $this->addCommand("git pull origin dev");
        }

        $this->addCommand("composer install");
        $this->addCommand("npm install");
        $this->addCommand("php app/console doctrine:schema:update --force");

        if ($this->env == "prod" || $this->env == "test") {
            $this->addCommand("php app/console cache:clear --env=prod");
        } else {
            $this->addCommand("php app/console cache:clear");
        }

        if ($continue) {
            $this->executeCommandString($output);
        }
        return;
    }

    private function executeCommandString() {
        $command = implode(" ; ", $this->commands);

        $process = new Process($command, null, null, null, 0);
        $process->run(function ($type, $buffer) {
            echo $buffer;
        });
        $process = null;
    }

    private function executeCommands() {
        foreach($this->commands as $command) {
            $process = new Process($command, null, null, null, 0);
            $process->run(function ($type, $buffer) {
                echo $buffer;
            });
            $process = null;
        }
    }

    private function addCommand($command) {
        $this->commands[] = $command;
    }
}


















